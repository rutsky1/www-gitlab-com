---
layout: markdown_page
title: "Administrative Coordinator"
---

## Responsibilities


* Assist in smooth onboarding of new team members.
* Help organize team events and provide support for team travel.
* Assist in running and improving recruiting and hiring processes.
* Help executives and other team members with purchases, vendors, gifts, reservations, 
and other miscellaneous tasks.
