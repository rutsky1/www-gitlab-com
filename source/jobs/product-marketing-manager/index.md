---
layout: markdown_page
title: "Product Marketing Manager"
---

## Responsibilities

* Manage monthly release post (lots of technical details and input from many teams required)
* Work with product and engineering to write feature highlights
* Work help technical writing and engineering to write documentation.
* Create sales enablement materials like white-papers and case studies.
* Website copy for the redesign of the website and copy for release
* Market and competitive research collaborate with other product people on what to build next
* Work with engineering and design on creating new ways to market (graphics or animations explaining complex new features)
* Manage expectations about upcoming features => some features are not shipped on time, this is normal, but marketing, sales, community have to be aware of this.
